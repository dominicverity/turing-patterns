/**
 * Project: 	Turing Pattern Generator
 * Comment:     A simple cellular automaton which simulates the generation of 
 * 				skin patterns in Turing style reaction-diffusion models.
 * References:	[1] Turing, A. The Chemical Basis of Morphogenesis, Philosophical
 * 					Transactions of the Royal Society B 237, 32 (1952)
 * 				[2] Young, D.A. A Local Activator-Inhibitor Model of Vertebrate
 * 					Skin Patterns, Mathematical Biosciences 71:51-58 (1984)
 *
 * This file is part of the sample code provided in the unit COMP229 "Object Oriented
 * Programming Practices".
 *
 * Copyright (C) 2012 Dominic Verity, Macquarie University.
 *
 * This is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 */

package org.macquarie.cellular;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Objects of this class represent neighbourhood templates for the updater
 * mechanism in a cellular automaton which simulates the development of
 * vertebrate skin patterns. This automaton implements a Turing-style 
 * activator-inhibitor diffusion-reaction model of morphogenesis.
 * 
 * More precisely, an object of this kind specifies the "morphogenic field"
 * that is generated around a differentiated cell. The field value at a given
 * point relative to this cell represents the aggregated concentration of
 * activator / inhibitor morphogen at that point. 
 * 
 * This field is positive (resp. negative) wherever activator concentrations 
 * are greater (resp. less) than inhibitor concentrations. 
 * 
 * @author Dominic Verity
 *
 */
public class MorphogenicField implements Iterable<Map.Entry<Position, Double>> {

	/*
	 * Data members
	 */
	
	/**
	 * <p>Hash map of field values.<p>
	 * <ul> 
	 * 	<li>Keys: positions on the automaton surface (given as values of type {@link Position}).</li>
	 *  <li>Values: morphogenic field strengths (given as <code>double</code>s).</li>
	 * </ul>
	 */
	private Map<Position, Double> mField;
	
	/*
	 * Constructors.
	 */
	
	/**
	 * <p>Default constructor. Constructs a null field.</p><br>
	 * <p>Made private so that instances may only be obtained using static
	 * factory methods.</p>
	 */
	private MorphogenicField() {
		mField = new HashMap<Position, Double>();
	}
	
	/*
	 * Static methods.
	 */
	
	/**
	 * Make a null field. This represents the "zero" field found 
	 * around a cell which is not manufacturing any morphogens.
	 * 
	 * @return the newly constructed null field object.
	 */
	public static MorphogenicField makeNullField() {
		return new MorphogenicField();
	}
	
	/**
	 * Make an elliptic field of a specified (constant) strength.
	 * 
	 * @param pHorizRadius horizontal radius of ellipse.
	 * @param pVerticalRadius vertical radius of ellipse.
	 * @param pHorizOffset horizontal offset of centre of ellipse from cell location.
	 * @param pVerticalOffset vertical offset of centre of ellipse from cell location.
	 * @param pStrength field strength.
	 * 
	 * @return the newly constructed elliptic field object.
	 */
	public static MorphogenicField makeEllipticField(
			double pHorizRadius, double pVerticalRadius, 
			double pHorizOffset, double pVerticalOffset,
			double pStrength) {
		
		MorphogenicField vResult = new MorphogenicField();

		int vStartRow = (int)Math.floor(pVerticalOffset - pVerticalRadius);
		int vEndRow = (int)Math.ceil(pVerticalOffset + pVerticalRadius);
		int vStartColumn = (int)Math.floor(pHorizOffset - pHorizRadius);
		int vEndColumn = (int)Math.ceil(pHorizOffset + pHorizRadius);
		
		for (int i = vStartRow; i <= vEndRow; i++) {
			for (int j = vStartColumn; j <= vEndColumn; j++) {
				if (Math.pow((j - pHorizOffset)/pHorizRadius, 2) + 
						Math.pow((i - pVerticalOffset)/pVerticalRadius, 2) <= 1) {
					vResult.mField.put(new Position(i,j), pStrength);
				}
			}
		}
		
		return vResult;
	}
	
	/**
	 * Make an elliptic field of a specified (constant) strength which is centred
	 * on the cell location.
	 * 
	 * @param pHorizRadius horizontal radius of ellipse.
	 * @param pVerticalRadius vertical radius of ellipse.
	 * @param pStrength field strength.
	 * 
	 * @return the newly constructed elliptic field object.
	 */
	public static MorphogenicField makeEllipticField(
			double pHorizRadius, double pVerticalRadius, double pStrength) {
		return makeEllipticField(pHorizRadius, pVerticalRadius, 0, 0, pStrength);
	}
	
	/**
	 * Make an circular field of a specified (constant) strength which is centred
	 * on the cell location.
	 * 
	 * @param pRadius radius of circle.
	 * @param pStrength field strength.
	 * 
	 * @return the newly constructed circular field object.
	 */
	public static MorphogenicField makeCircularField(double pRadius, double pStrength) {
		return makeEllipticField(pRadius, pRadius, pStrength);
	}
	
	/*
	 * Methods.
	 */

	/**
	 * Make a new field by superposing two fields.
	 *
	 * @param pOther the field to superpose onto this one.
	 * @return a new field obtained by superposing this one and pOther.
	 */
	public MorphogenicField superposeWith(MorphogenicField pOther) {
		MorphogenicField vResult = new MorphogenicField();
		vResult.mField.putAll(mField);
		for (Map.Entry<Position, Double> vEntry : pOther) {
			Position vPosition = vEntry.getKey();
			Double vStrength = vEntry.getValue();
			if (mField.containsKey(vPosition)) {
				vResult.mField.put(vPosition, mField.get(vPosition) + vStrength);
			} else {
				vResult.mField.put(vPosition, vStrength);
			}
		}
		return vResult;
	}

	/**
	 * Make a new field by masking this one with a second field.
	 *
	 * @param pOther the field to mask onto this one.
	 * @return a new field obtained by copying this one and masking pOther onto it.
	 */
	public MorphogenicField maskBy(MorphogenicField pOther) {
		MorphogenicField vResult = new MorphogenicField();
		vResult.mField.putAll(mField);
		vResult.mField.putAll(pOther.mField);
		return vResult;
	}

	/**
	 * Get field strength at specified point relative to a cell.
	 * 
	 * @param pPos the position, relative to a cell, to obtain the field strength from.
	 * @return the strength of field developed by a cell at the specified point.
	 */
	public double getFieldStrength(Position pPos) {
		if (mField.containsKey(pPos)) {
			return mField.get(pPos);
		} else {
			return 0;
		}
	}
	
	/**
	 * Get field strength at specified point relative to a cell.
	 * 
	 * @param pRow the row number, relative to a cell, to obtain the field strength from.
	 * @param pColumn the column number, relative to a cell, to obtain the field strength from.
	 * @return the strength of field developed by a cell at the specified point.
	 */
	public double getFieldStrength(int pRow, int pColumn) {
		return getFieldStrength(new Position(pRow,pColumn));
	}
	
	/**
	 * Iterator for traversals over the non-zero values in the field generated by a cell.
	 */
	public Iterator<Entry<Position,Double>> iterator() {
		return mField.entrySet().iterator();
	}
}
