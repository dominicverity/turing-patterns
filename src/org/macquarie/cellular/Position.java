/**
 * Project: 	Turing Pattern Generator
 * Comment:     A simple cellular automaton which simulates the generation of 
 * 				skin patterns in Turing style reaction-diffusion models.
 * References:	[1] Turing, A. The Chemical Basis of Morphogenesis, Philosophical
 * 					Transactions of the Royal Society B 237, 32 (1952)
 * 				[2] Young, D.A. A Local Activator-Inhibitor Model of Vertebrate
 * 					Skin Patterns, Mathematical Biosciences 71:51-58 (1984)
 *
 * This file is part of the sample code provided in the unit COMP229 "Object Oriented
 * Programming Practices".
 *
 * Copyright (C) 2012 Dominic Verity, Macquarie University.
 *
 * This is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 */

package org.macquarie.cellular;

/**
 * Objects of this class describe positions on a life board as a 
 * pair of a row and a column number.
 *
 * @author Dominic Verity
 *
 */
public class Position {
	/*
	 * Instance variables (data members / fields)
	 */
	/**
	 * A position on the life board is given as a pair
	 * of a row number and a column number.
	 */
	private int mRow, mColumn;
	
	/*
	 * Constructors
	 */
	/**
	 * Construct a new position from a given row and 
	 * column number.
	 * 
	 * @param pRow the row of the new position.
	 * @param pColumn the column of the new position.
	 */
	Position(int pRow, int pColumn) {
		mRow = pRow;
		mColumn = pColumn;
	}
	
	/*
	 * Methods
	 */
	/**
	 * Row number getter.
	 *
	 * @return the row number of this Position.
	 */
	int getRow() {
		return mRow;
	}
	
	/**
	 * Column number getter.
	 *
	 * @return the column number of this Position.
	 */
	int getColumn() {
		return mColumn;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pOther) {
		if (pOther instanceof Position) {
			Position vOther = (Position)pOther;
			return ((mRow == vOther.mRow) && (mColumn == vOther.mColumn));
		} else
			return false;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 7 * mRow + 11 * mColumn;
	}
}
