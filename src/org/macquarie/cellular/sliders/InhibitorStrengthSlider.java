/**
 * Project: 	Turing Pattern Generator
 * Comment:     A simple cellular automaton which simulates the generation of 
 * 				skin patterns in Turing style reaction-diffusion models.
 * References:	[1] Turing, A. The Chemical Basis of Morphogenesis, Philosophical
 * 					Transactions of the Royal Society B 237, 32 (1952)
 * 				[2] Young, D.A. A Local Activator-Inhibitor Model of Vertebrate
 * 					Skin Patterns, Mathematical Biosciences 71:51-58 (1984)
 *
 * This file is part of the sample code provided in the unit COMP229 "Object Oriented
 * Programming Practices".
 *
 * Copyright (C) 2012 Dominic Verity, Macquarie University.
 *
 * This is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 */

package org.macquarie.cellular.sliders;

/**
 * Custom slider component which is specialised to allow the setting of the
 * field strength of the inhibitor region. Such sliders have a tick size of 
 * -0.001 units, a minimum value of -0.05 and a maximum value of -0.3. 
 * 
 * @author Dominic Verity
  *
 */
@SuppressWarnings("serial")
public class InhibitorStrengthSlider extends CustomSlider {

	/**
	 * Make an inhibitor strength slider object with specified caption and initial value.
	 * 
	 * @param pDefault default (double) value to initialise the new slider to.
	 * @param pCaption text caption to place in the label above the new slider.
	 */
	public InhibitorStrengthSlider(double pDefault, String pCaption) {
		super(-0.05, pDefault, pCaption, -1000, 5, 50);
	}


	/* (non-Javadoc)
	 * @see org.macquarie.cellular.sliders.CustomSlider#formatValue(double)
	 */
	@Override
	protected String formatValue(double pValue) {
		return String.format("%.3f", pValue);
	}

}
