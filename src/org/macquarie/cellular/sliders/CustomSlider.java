/**
 * Project: 	Turing Pattern Generator
 * Comment:     A simple cellular automaton which simulates the generation of 
 * 				skin patterns in Turing style reaction-diffusion models.
 * References:	[1] Turing, A. The Chemical Basis of Morphogenesis, Philosophical
 * 					Transactions of the Royal Society B 237, 32 (1952)
 * 				[2] Young, D.A. A Local Activator-Inhibitor Model of Vertebrate
 * 					Skin Patterns, Mathematical Biosciences 71:51-58 (1984)
 *
 * This file is part of the sample code provided in the unit COMP229 "Object Oriented
 * Programming Practices".
 *
 * Copyright (C) 2012 Dominic Verity, Macquarie University.
 *
 * This is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this code.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 * 
 */

package org.macquarie.cellular.sliders;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Dictionary;
import java.util.Hashtable;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * A custom widget class which implements the functionality of the sliders
 * used to adjust the morphogenic field of a differentiated cell.
 * 
 * @author Dominic Verity
 *
 */
@SuppressWarnings("serial")
public abstract class CustomSlider extends JPanel implements ChangeListener {

	private double mMinimum;
	private int mDefault;
	private double mValue;
	private String mCaption;
	private Dictionary<Integer, JLabel> mLabels;
	private JSlider mSlider;
	private JTextField mValueField;
	private int mNumMinorTicks;
	private int mNumMinorsPerMajorTick;
	private int mTicksPerUnit;

	// Constructors
	
	/**
	 * Make a custom slider object with specified caption, tick value, range and initial value.
	 * 
	 * @param pMinimum minimum (double) value to which the new slider can be set. 
	 * @param pDefault default (double) value to initialise the new slider to.
	 * @param pCaption text caption to place in the label above the new slider.
	 * @param pTicksPerUnit number of minor ticks on the new slider which together represent the value 1.0d.
	 * @param pNumMajorTicks number of major ticks to be placed on the new slider.
	 * @param pNumMinorsPerMajorTick number of minor ticks between each pair of major ticks on the new slider. 
	 */
	public CustomSlider(
			double pMinimum, double pDefault, String pCaption, 
			int pTicksPerUnit, int pNumMajorTicks, int pNumMinorsPerMajorTick) {
		mCaption = pCaption;
		mMinimum = pMinimum;
		mTicksPerUnit = pTicksPerUnit;
		mNumMinorsPerMajorTick = pNumMinorsPerMajorTick;
		mNumMinorTicks = pNumMajorTicks * pNumMinorsPerMajorTick;
		
		mLabels = new Hashtable<Integer, JLabel>();
		
		for (int i = 0; i <= mNumMinorTicks; i += pNumMinorsPerMajorTick) {
			mLabels.put(i, new JLabel(formatValue(mMinimum + (double)i / mTicksPerUnit)));
		}
		
		mDefault = (int)Math.round((pDefault - mMinimum) * mTicksPerUnit);
		mValue = pMinimum + (double)mDefault / mTicksPerUnit;
		
		setup();
	}
	
	/**
	 * Create the component parts of this widget an lay them out
	 * in a panel.
	 */
	private void setup() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		// Create a panel to contain the label and the current value field.
		JPanel vLabelPanel = new JPanel();
		vLabelPanel.setLayout(new BoxLayout(vLabelPanel, BoxLayout.X_AXIS));
		vLabelPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		// Build the label.
		JLabel vLabel = new JLabel(mCaption);
		
		// Build the current value text field.
		mValueField = new JTextField();
		mValueField.setEditable(false);
		mValueField.setFocusable(false);
		mValueField.setText(formatValue(mValue));
		
		Dimension vSize = new Dimension(60,20);
		mValueField.setMaximumSize(vSize);
		mValueField.setPreferredSize(vSize);
		
		// Add label and current value field to label panel.
		vLabelPanel.add(vLabel);
		vLabelPanel.add(Box.createHorizontalStrut(2));
		vLabelPanel.add(mValueField);
		
		// Build the slider itself
		mSlider = new JSlider(JSlider.HORIZONTAL, 0, mNumMinorTicks, mDefault); 
		mSlider.setMajorTickSpacing(mNumMinorsPerMajorTick);
		mSlider.setPaintTicks(true);
		mSlider.setLabelTable(mLabels);
		mSlider.setPaintLabels(true);
		mSlider.addChangeListener(this);
		mSlider.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		add(vLabelPanel);
		add(mSlider);
		add(Box.createVerticalStrut(10));
	}
	
	/**
	 * @return the current (scaled, double) value that this slider is set to.
	 */
	public double getValue() {
		return mValue;
	}
	
	/**
	 * This method is overridden in subclasses in order to specify how the labels
	 * on the slider and the value in the associated status text field are to
	 * be formatted as strings for display.
	 */
	abstract protected String formatValue(double pValue);
	

	/* (non-Javadoc)
	 * @see java.awt.Component#toString()
	 */
	@Override
	public String toString() {
		return formatValue(mValue);
	}

	/**
	 * Update the contents of the text field displaying the current scaled value of this
	 * component whenever the slider is moved.
	 * 
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent pE) {
		mValue = mMinimum + (double)mSlider.getValue() / mTicksPerUnit;
		mValueField.setText(formatValue(mValue));
	}

}
